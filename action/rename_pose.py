
import argparse
import bpy
import json
import re
import sys

from pathlib import Path
#sys.path.append(str(Path(__file__).parents[3]))
from asset_library.common.bl_utils import (
    get_preview,
)

def rename_pose(src_name='', dst_name=''):
        
    scn = bpy.context.scene
    action = bpy.data.actions.get(src_name)
    if not action:
        print(f'No {src_name} not found.')
        bpy.ops.wm.quit_blender()

    action.name = dst_name
    preview = get_preview(asset_path=bpy.data.filepath, asset_name=src_name)
    if preview:
        preview.rename(re.sub(src_name, dst_name, str(preview)))

    bpy.ops.wm.save_mainfile(
        filepath=bpy.data.filepath, compress=True, exit=True
    )
    

if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Add Comment To the tracker',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--src-name')
    parser.add_argument('--dst-name')

    if '--' in sys.argv :
        index = sys.argv.index('--')
        sys.argv = [sys.argv[index-1], *sys.argv[index+1:]]

    args = parser.parse_args()
    rename_pose(**vars(args))
