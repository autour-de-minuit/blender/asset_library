
import bpy


def draw_context_menu(layout):
    params = bpy.context.space_data.params
    asset = bpy.context.asset_file_handle

    layout.operator("assetlib.open_blend", text="Open blend file")#.asset = asset.name
    layout.operator("assetlib.play_preview", text="Play Preview")
    
    layout.separator()

    layout.operator_context = 'INVOKE_DEFAULT'

    #layout.operator("assetlib.rename_asset", text="Rename Action")
    layout.operator("assetlib.remove_assets", text="Remove Assets")
    layout.operator("assetlib.edit_data", text="Edit Asset data")
    
    #layout.operator("actionlib.clear_asset", text="Clear Asset (Fake User)").use_fake_user = True

    layout.separator()

    layout.operator("actionlib.apply_selected_action", text="Apply Pose").flipped = False
    layout.operator("actionlib.apply_selected_action", text="Apply Pose (Flipped)").flipped = True
    
    layout.separator()

    layout.operator("poselib.blend_pose_asset_for_keymap", text="Blend Pose").flipped = False
    layout.operator("poselib.blend_pose_asset_for_keymap", text="Blend Pose (Flipped)").flipped = True

    layout.separator()

    layout.operator("poselib.pose_asset_select_bones", text="Select Bones").selected_side = 'CURRENT'
    layout.operator("poselib.pose_asset_select_bones", text="Select Bones (Flipped)").selected_side = 'FLIPPED'
    layout.operator("poselib.pose_asset_select_bones", text="Select Bones (Both)").selected_side = 'BOTH'

    layout.separator()
    #layout.operator("asset.library_refresh")
    if params.display_type == 'THUMBNAIL':
        layout.prop_menu_enum(params, "display_size")


def draw_header(layout):
    '''Draw the header of the Asset Browser Window'''

    layout.separator()
    layout.operator("actionlib.store_anim_pose", text='Add Action', icon='FILE_NEW')

