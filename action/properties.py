import bpy
from bpy.types import PropertyGroup
from bpy.props import PointerProperty, StringProperty, BoolProperty

class ACTIONLIB_PG_scene(PropertyGroup):
    flipped : BoolProperty(
        name="Flip Pose",
        default=False,
    )
    previous_action : PointerProperty(type=bpy.types.Action)
    publish_path : StringProperty(subtype='FILE_PATH')
    camera : PointerProperty(type=bpy.types.Object, poll=lambda s, o: o.type == 'CAMERA')
    rest_pose : PointerProperty(type=bpy.types.Action, poll=lambda s, a: a.asset_data)


classes = (
    ACTIONLIB_PG_scene,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.actionlib = PointerProperty(type=ACTIONLIB_PG_scene)

def unregister():
    try:
        del bpy.types.Scene.actionlib
    except AttributeError:
        pass

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)