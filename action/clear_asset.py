
import argparse
import bpy
import json
import sys

from pathlib import Path
#sys.path.append(str(Path(__file__).parents[3]))
from asset_library.common.bl_utils import (
    get_preview,
)

def clear_asset(action_name='', use_fake_user=False):
        
    scn = bpy.context.scene

    action = bpy.data.actions.get(action_name)
    if not action:
        print(f'No {action_name} not found.')
        bpy.ops.wm.quit_blender()

    action.asset_clear()    
    if use_fake_user:
        action.use_fake_user = True
    else:
        preview = get_preview(asset_path=bpy.data.filepath, asset_name=action_name)
        if preview:
            preview.unlink()
        bpy.data.actions.remove(action)
        
    bpy.ops.wm.save_mainfile(
        filepath=bpy.data.filepath, compress=True, exit=True
    )
    
    
if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Add Comment To the tracker',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--action-name')
    parser.add_argument('--use-fake-user', type=json.loads, default='false')

    if '--' in sys.argv :
        index = sys.argv.index('--')
        sys.argv = [sys.argv[index-1], *sys.argv[index+1:]]

    args = parser.parse_args()
    clear_asset(**vars(args))
