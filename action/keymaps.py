

from typing import List, Tuple

import bpy

addon_keymaps: List[Tuple[bpy.types.KeyMap, bpy.types.KeyMapItem]] = []

def register():
    wm = bpy.context.window_manager
    addon = wm.keyconfigs.addon
    if not addon:
        return
    
    km = addon.keymaps.new(name="File Browser Main", space_type="FILE_BROWSER")

    # DblClick to apply pose.
    kmi = km.keymap_items.new("actionlib.apply_selected_action", "LEFTMOUSE", "DOUBLE_CLICK")
    kmi.properties.flipped = False
    addon_keymaps.append((km, kmi))
        
    kmi = km.keymap_items.new("actionlib.apply_selected_action", "LEFTMOUSE", "DOUBLE_CLICK", alt=True)
    kmi.properties.flipped = True
    addon_keymaps.append((km, kmi))
        
    kmi = km.keymap_items.new("poselib.blend_pose_asset_for_keymap", "LEFTMOUSE", "DOUBLE_CLICK", shift=True)
    kmi.properties.flipped = False
    addon_keymaps.append((km, kmi))
    
    kmi = km.keymap_items.new("poselib.blend_pose_asset_for_keymap", "LEFTMOUSE", "DOUBLE_CLICK", alt=True, shift=True)
    kmi.properties.flipped = True
    addon_keymaps.append((km, kmi))
    
    kmi = km.keymap_items.new("poselib.pose_asset_select_bones", "S", "PRESS")
    kmi.properties.selected_side = 'CURRENT'
    addon_keymaps.append((km, kmi))

    kmi = km.keymap_items.new("poselib.pose_asset_select_bones", "S", "PRESS", alt=True)
    kmi.properties.selected_side = 'FLIPPED'
    addon_keymaps.append((km, kmi))

    kmi = km.keymap_items.new("poselib.pose_asset_select_bones", "S", "PRESS", alt=True, ctrl=True)
    kmi.properties.selected_side = 'BOTH'
    addon_keymaps.append((km, kmi))

def unregister():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()