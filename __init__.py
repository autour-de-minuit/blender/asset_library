# SPDX-License-Identifier: GPL-2.0-or-later


"""
Extending features of the Asset Browser for a studio use. 
"""

bl_info = {
    "name": "Asset Library",
    "description": "Asset Library based on the Asset Browser.",
    "author": "Sybren A. Stüvel, Clement Ducarteron, Christophe Seux, Samuel Bernou",
    "version": (2, 0),
    "blender": (3, 3, 0),
    "warning": "In development, things may change",
    "location": "Asset Browser -> Animations, and 3D Viewport -> Animation panel",
    "category": "Animation",
}

#from typing import List, Tuple


from asset_library import pose
from asset_library import action
from asset_library import collection
from asset_library import file
from asset_library import (gui, keymaps, preferences, operators)
from asset_library import constants
#from asset_library.common.library_type import LibraryType
from asset_library.common.bl_utils import get_addon_prefs
from asset_library.common.functions import set_env_libraries
from asset_library.common.template import Template

import re


if 'bpy' in locals():
    print("Reload Addon Asset Library")

    import importlib

    importlib.reload(constants)
    importlib.reload(gui)
    importlib.reload(keymaps)

    importlib.reload(preferences)
    importlib.reload(operators)
    importlib.reload(constants)

    importlib.reload(action)
    importlib.reload(file)
    importlib.reload(collection)

import bpy
import os


#addon_keymaps: List[Tuple[bpy.types.KeyMap, bpy.types.KeyMapItem]] = []

bl_modules = (
    operators,
    pose,
    action,
    collection,
    file,
    keymaps,
    gui,
    preferences
)


def load_handler():
    print('load_handler')

    set_env_libraries()
    bpy.ops.assetlib.set_paths(all=True)

    if not bpy.app.background:
        bpy.ops.assetlib.bundle(blocking=False, mode='AUTO_BUNDLE')
            


def register() -> None:
    

    for m in bl_modules:
        m.register()

    #prefs = get_addon_prefs()
    



    bpy.app.timers.register(load_handler, first_interval=1)


def unregister() -> None:
    prefs = get_addon_prefs()
    bpy.utils.previews.remove(prefs.previews)

    for m in reversed(bl_modules):
        m.unregister()
    
    
