
from asset_library.collection import (
    gui,
    operators,
    keymaps,
    #build_collection_blends,
    #create_collection_library,
    )

if 'bpy' in locals():
    import importlib

    importlib.reload(gui)
    importlib.reload(operators)
    importlib.reload(keymaps)
    #importlib.reload(build_collection_blends)
    #importlib.reload(create_collection_library)

import bpy

def register():
    operators.register()
    keymaps.register()

def unregister():
    operators.unregister()
    keymaps.unregister()