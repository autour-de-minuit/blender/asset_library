

from typing import List, Tuple

import bpy

addon_keymaps: List[Tuple[bpy.types.KeyMap, bpy.types.KeyMapItem]] = []

def register():
    wm = bpy.context.window_manager
    addon = wm.keyconfigs.addon
    if not addon:
        return

    km = addon.keymaps.new(name="File Browser Main", space_type="FILE_BROWSER")
    kmi = km.keymap_items.new("assetlib.load_asset", "LEFTMOUSE", "DOUBLE_CLICK") # , shift=True
    addon_keymaps.append((km, kmi))

def unregister():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()