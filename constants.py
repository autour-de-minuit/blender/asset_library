from pathlib import Path
import bpy


DATA_TYPE_ITEMS = [
    ("ACTION", "Action", "", "ACTION", 0),
    ("COLLECTION", "Collection", "", "OUTLINER_OB_GROUP_INSTANCE", 1),
    ("FILE", "File", "", "FILE", 2)
]
DATA_TYPES = [i[0] for i in DATA_TYPE_ITEMS]
ICONS = {identifier: icon for identifier, name, description, icon, number in DATA_TYPE_ITEMS}

ASSETLIB_FILENAME = "blender_assets.libs.json"
MODULE_DIR = Path(__file__).parent
RESOURCES_DIR = MODULE_DIR / 'resources'

LIBRARY_TYPE_DIR = MODULE_DIR / 'library_types'
LIBRARY_TYPES = []

ADAPTER_DIR = MODULE_DIR / 'adapters'
ADAPTERS = []

PREVIEW_ASSETS_SCRIPT = MODULE_DIR / 'common' / 'preview_assets.py'

#ADD_ASSET_DICT = {}

