
import bpy
from pathlib import Path

from bpy.types import (
    Context,
    Header,
    Menu,
    Panel,
    UIList,
    WindowManager,
    WorkSpace,
)

from bpy_extras import asset_utils
from asset_library.common.bl_utils import get_object_libraries, get_addon_prefs
from asset_library.common.functions import get_active_library


def draw_context_menu(layout):
    #asset = context.active_file
    layout.operator_context = "INVOKE_DEFAULT"
    lib = get_active_library()
    filepath = lib.library_type.get_active_asset_path()

    layout.operator("assetlib.open_blend_file", text="Open Blend File")#.filepath = asset.asset_data['filepath']
    op = layout.operator("wm.link", text="Link")
    op.filepath = str(filepath)
    
    op = layout.operator("wm.append", text="Append")
    op.filepath = str(filepath)


def draw_header(layout):
    '''Draw the header of the Asset Browser Window'''

    layout.separator()
    #layout.operator("actionlib.store_anim_pose", text='Add Action', icon='FILE_NEW')