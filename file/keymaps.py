

from typing import List, Tuple

import bpy
from bpy.app.handlers import persistent

addon_keymaps: List[Tuple[bpy.types.KeyMap, bpy.types.KeyMapItem]] = []

def register() -> None:
    wm = bpy.context.window_manager
    if not wm.keyconfigs.addon:
        # This happens when Blender is running in the background.
        return

    km = wm.keyconfigs.addon.keymaps.new(name="File Browser Main", space_type="FILE_BROWSER")

    kmi = km.keymap_items.new("assetlib.open_blend_file", "LEFTMOUSE", "DOUBLE_CLICK")
    addon_keymaps.append((km, kmi))


def unregister() -> None:
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()