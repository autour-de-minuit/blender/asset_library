

from bpy.types import PropertyGroup


class Adapter(PropertyGroup):

    #def __init__(self):
    name = "Base Adapter"
    #library = None
    def to_dict(self):
        return {p: getattr(self, p) for p in self.bl_rna.properties.keys() if p !='rna_type'}