
"""
Adapter for making an asset library of all  blender file found in a folder
"""


from asset_library.library_types.library_type import LibraryType
from asset_library.common.file_utils import copy_dir
from bpy.props import StringProperty
from os.path import expandvars
import bpy


class CopyFolder(LibraryType):
    """Copy library folder from a server to a local disk for better performance"""

    name = "Copy Folder"
    source_directory : StringProperty()
    
    includes : StringProperty() 
    excludes : StringProperty() 

    def bundle(self, cache_diff=None):
        src = expandvars(self.source_directory)
        dst = expandvars(self.bundle_directory)

        includes = [inc.strip() for inc in self.includes.split(',')]
        excludes = [ex.strip() for ex in self.excludes.split(',')]

        print(f'Copy Folder from {src} to {dst}...')
        copy_dir(
            src, dst, only_recent=True,
            excludes=excludes, includes=includes
        )
    
    def filter_prop(self, prop):
        if prop in ('template_info', 'template_video', 'template_image', 'blend_depth'):
            return False
        
        return True

    # def draw_prop(self, layout, prop):
    #     if prop in ('template_info', 'template_video', 'template_image', 'blend_depth'):
    #         return

    #     super().draw_prop(layout)