
import argparse
import fnmatch
import importlib.util
import json
import re
import subprocess
import sys

from pathlib import Path

# import module utils without excuting __init__
spec = importlib.util.spec_from_file_location(
    "utils", Path(__file__).parent/"file_utils.py"
)
utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(utils)


def synchronize(src, dst, only_new=False, only_recent=False):

    excludes=['*.sync-conflict-*', '.*']
    includes=['*.blend', 'blender_assets.cats.txt']
    
    utils.copy_dir(
        src, dst,
        only_new=only_new, only_recent=only_recent,
        excludes=excludes, includes=includes
    )


if __name__ == '__main__' :
    parser = argparse.ArgumentParser(description='Add Comment To the tracker',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--src')
    parser.add_argument('--dst')
    parser.add_argument('--only-new', type=json.loads, default='false')
    parser.add_argument('--only-recent', type=json.loads, default='false')

    args = parser.parse_args()
    synchronize(**vars(args))
