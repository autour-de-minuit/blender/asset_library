

from asset_library.common import file_utils
from asset_library.common import functions
from asset_library.common import synchronize
from asset_library.common import template
from asset_library.common import catalog

if 'bpy' in locals():
    import importlib

    importlib.reload(file_utils)
    importlib.reload(functions)
    importlib.reload(synchronize)
    importlib.reload(template)
    importlib.reload(catalog)

import bpy